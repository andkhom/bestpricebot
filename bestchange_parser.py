import requests
import zipfile
import pandas as pd


class BestChange:
    def __init__(self, coin=88,*args):
        self.coin = coin
        self.ids = args
        r = requests.get('http://www.bestchange.ru/bm/info.zip')
        with open('bestchange.zip', 'wb') as myzip:
            myzip.write(r.content)
        z = zipfile.ZipFile('bestchange.zip')
        z.extractall()
        df1 = pd.read_csv('bm_rates.dat', ';',
                          names=['id_from', 'id_to', 'id_exchange', 'price', 'price1', 'x0', 'x1', 'x2'])
        df2 = pd.read_csv('bm_exch.dat', ';', encoding='cp1251', names=['id_exchange', 'name', 'x1', 'x2', 'x3'])
        res = df1.merge(df2, on='id_exchange')
        self.res = res[['id_from', 'id_to', 'name', 'price', 'price1']]

    def get_coin_names(self):
        from_data = {
            'id_from': [93, 139, 172, 99, 140, 88],
            'fr': ['BTC', 'ETH', 'BCH', 'LTC', 'DASH', 'USD']
        }
        fromdf = pd.DataFrame(from_data, columns=['id_from', 'fr'])
        to_data = {
            'id_to': [93, 139, 172, 99, 140, 88],
            'to': ['BTC', 'ETH', 'BCH', 'LTC', 'DASH', 'USD']
        }
        todf = pd.DataFrame(to_data, columns=['id_to', 'to'])
        self.res = self.res.merge(todf, on='id_to').merge(fromdf, on='id_from')

    def data(self):
        self.get_coin_names()
        buy = self.res.loc[(self.res['id_from'] == self.coin) & self.res['id_to'].isin(self.ids)]
        buy = buy[['name', 'price', 'fr', 'to']]
        buy['type'] = 'exchange'
        sell = self.res.loc[self.res['id_from'].isin(self.ids) & (self.res['id_to'] == self.coin)]
        sell = sell[['name', 'price1', 'fr', 'to']]
        sell.rename(columns={'price1': 'price'}, inplace=True)
        sell['type'] = 'exchange'
        return {'sell': sell, 'buy': buy}


