import requests
import pandas as pd
from random import choice, randint
from time import sleep


class MarketParser:
    def __init__(self, url, **kwargs):
        self.url = url
        self.pairs_dict = {v: k for k, v in kwargs.items()}
        self.buy = {'name': [], 'price': [], 'to': [], 'fr': []}
        self.sell = {'name': [], 'price': [], 'to': [], 'fr': []}

    def get_proxy_request(self, pair, name):
        useragents = open('user-agent.txt').read().split('\n')
        proxies = open('proxy.txt').read().split('\n')
        print('proxy')
        try:
            pr = choice(proxies)
            proxy = {
                'https': 'https://' + pr
            }
            useragent = {'User-Agent': choice(useragents)}
            r = requests.post(self.url + pair, headers=useragent, proxies=proxy)
            trades = r.json()
        except:
            print("Connection refused by the server {}".format(name))
            trades = None
        return trades

    def get_request(self, pair, name):
        try:
            r = requests.get(self.url + pair)
            trades = r.json()
        except:
            # sleep(randint(1,4))
            trades = self.get_proxy_request(pair, name)
        return trades

    def get_dict(self, type, **kwargs):
        for kwarg in kwargs:
            self.__dict__[type][kwarg].append(kwargs[kwarg])


class YobitParser(MarketParser):
    def get_prices(self):
        key = '-'.join(self.pairs_dict.keys())
        trades = self.get_request(key, 'yobit.com')
        if trades:
            for pair in self.pairs_dict:
                buy_price = list(filter(lambda x: x['type'] == 'bid', trades[pair]))[0]['price']
                self.get_dict('buy', name='Yobit', price=float(buy_price), fr='USD', to=self.pairs_dict[pair])
                sell_price = list(filter(lambda x: x['type'] == 'ask', trades[pair]))[0]['price']
                self.get_dict('sell', name='Yobit', price=float(sell_price), fr=self.pairs_dict[pair], to='USD')
        buy = pd.DataFrame.from_dict(self.buy)
        sell = pd.DataFrame.from_dict(self.sell)
        return {'buy': buy, 'sell': sell}


class ExmoParser(MarketParser):
    def get_prices(self):
        key = ','.join(self.pairs_dict.keys())
        trades = self.get_request(key, 'exmo.com')
        if trades:
            for pair in self.pairs_dict:
                buy_price = list(filter(lambda x: x['type'] == 'buy', trades[pair]))[0]['price']
                self.get_dict('buy', name='EXMO', price=float(buy_price), fr='USD', to=self.pairs_dict[pair])
                sell_price = list(filter(lambda x: x['type'] == 'sell', trades[pair]))[0]['price']
                self.get_dict('sell', name='EXMO', price=float(sell_price), fr=self.pairs_dict[pair], to='USD')
        buy = pd.DataFrame.from_dict(self.buy)
        sell = pd.DataFrame.from_dict(self.sell)
        return {'buy': buy, 'sell': sell}
