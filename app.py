import pandas as pd
import db


class DataHandler:
    def __init__(self, id, bs, *args):
        self.user = id
        self.buy, self.sell = bs['buy'], bs['sell']
        for arg in args:
            b, s = arg['buy'], arg['sell']
            b['type'], s['type'] = 'market', 'market'
            self.buy = self.buy.append(b, ignore_index=True)
            self.sell = self.sell.append(s, ignore_index=True)

    def best(self, type, p_type):
        df = self.__dict__[type].loc[self.__dict__[type]['type'] == p_type]
        new_df = pd.DataFrame()
        if type == 'buy':
            g_type = 'to'
        else:
            g_type = 'fr'
        for name, group in df.groupby([g_type]):
            if type == 'buy':
                group = group[group['price'] == group['price'].min()]
            else:
                group = group[group['price'] == group['price'].max()]
            new_df = new_df.append(group)
        new_df = new_df[['name', 'price', g_type]]
        new_df = new_df.rename(columns={'name': g_type, 'price': type, g_type: 'coin'})
        return new_df

    def recommend(self):
        buy_ex = self.best('buy', 'exchange')
        buy_ma = self.best('buy', 'market')
        sell_ex = self.best('sell', 'exchange')
        sell_ma = self.best('sell', 'market')

        res = buy_ex.merge(sell_ma, on='coin')
        res1 = buy_ma.merge(sell_ex, on='coin')
        res = res.append(res1)
        res['profit'] = (1 - (res['buy'] / res['sell']))*100
        profit = 0
        # profit = db.get_profit(self.user)
        # print(profit)
        # try:
        #     profit = int(profit[0][0])
        # except:
        #     profit = 0
        # if not profit:
        #     try:
        #         profit = db.add_user_to_db(self.user)
        #     except:
        #         profit = 0
        res = res[res['profit'] > profit]
        messages = []
        for index, row in res.iterrows():
            message = 'Монета {}. Купить на {} - {} $. Продать на {} - {} $. Profit - {} %.'.format(
                row['coin'], row['fr'], row['buy'], row['to'], row['sell'], round(row['profit'], 2))
            messages.append(message)
        return messages


# if __name__ == '__main__':
#
#     bs = BestChange(88, 93, 139, 172, 99, 140)
#     bs = bs.data()
#     yobit = YobitParser('https://yobit.net/api/3/trades/', BTC='btc_usd', ETH='eth_usd', LTC='ltc_usd', DASH='dash_usd')
#     yo = yobit.get_prices()
#     exmo = ExmoParser('https://api.exmo.com/v1/trades/?pair=', BTC='BTC_USD', ETH='ETH_USD', LTC='LTC_USD',
#                       DASH='DASH_USD')
#     ex = exmo.get_prices()
#     data = DataHandler(bs, yo, ex)
#     data.best()
#     print(data.recommend())
#     print(data.best_exchange())
#     print(data.best_market())

