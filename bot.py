import config
import db
import telebot
from bestchange_parser import BestChange
import flask
from flask import Flask
from markets_parser import YobitParser, ExmoParser
from app import DataHandler
import time
from config import HOST

bs = BestChange(88, 93, 139, 172, 99, 140)
bs = bs.data()
yobit = YobitParser('https://yobit.net/api/3/trades/', BTC='btc_usd', ETH='eth_usd', LTC='ltc_usd', DASH='dash_usd')
yo = yobit.get_prices()
exmo = ExmoParser('https://api.exmo.com/v1/trades/?pair=', BTC='BTC_USD', ETH='ETH_USD', LTC='LTC_USD',
                  DASH='DASH_USD')
ex = exmo.get_prices()
data = DataHandler(1, bs, yo, ex)
messages = data.recommend()

WEBHOOK_HOST = HOST
WEBHOOK_PORT = 443  # 443, 80, 88 or 8443 (port need to be 'open')
WEBHOOK_LISTEN = '0.0.0.0'  # In some VPS you may need to put here the IP addr

WEBHOOK_SSL_CERT = './webhook_cert.pem'  # Path to the ssl certificate
WEBHOOK_SSL_PRIV = './webhook_pkey.pem'  # Path to the ssl private key

# Quick'n'dirty SSL certificate generation:
#
# openssl genrsa -out webhook_pkey.pem 2048
# openssl req -new -x509 -days 3650 -key webhook_pkey.pem -out webhook_cert.pem
#
# When asked for "Common Name (e.g. server FQDN or YOUR name)" you should reply
# with the same value in you put in WEBHOOK_HOST

WEBHOOK_URL_BASE = "https://%s:%s" % (WEBHOOK_HOST, WEBHOOK_PORT)
WEBHOOK_URL_PATH = "/%s/" % (config.TELEGRAM_TOKEN)

app = Flask(__name__)

bot = telebot.TeleBot(config.TELEGRAM_TOKEN)


@app.route('/', methods=['GET', 'HEAD'])
def index():
    return ''


# Process webhook calls
@app.route(WEBHOOK_URL_PATH, methods=['POST'])
def webhook():
    if flask.request.headers.get('content-type') == 'application/json':
        json_string = flask.request.get_data().decode('utf-8')
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        return ''
    else:
        flask.abort(403)


@bot.message_handler(commands=['start'])
def get_greeting(message):
    chat_id = message.chat.id
    try:
        db.add_user_to_db(chat_id)
    except:
        print('Пользователь уже создан')
    bot.send_message(chat_id, 'Привет, для получения информации используй команду /info.')
    bot.send_message(chat_id, 'Для получения списка всех комманд используй /help.')


@bot.message_handler(commands=['info'])
def get_start(message):
    chat_id = message.chat.id
    bs = BestChange(88, 93, 139, 172, 99, 140)
    bs = bs.data()
    yobit = YobitParser('https://yobit.net/api/3/trades/', BTC='btc_usd', ETH='eth_usd', LTC='ltc_usd', DASH='dash_usd')
    yo = yobit.get_prices()
    exmo = ExmoParser('https://api.exmo.com/v1/trades/?pair=', BTC='BTC_USD', ETH='ETH_USD', LTC='LTC_USD',
                      DASH='DASH_USD')
    ex = exmo.get_prices()
    data = DataHandler(chat_id, bs, yo, ex)
    messages = data.recommend()
    if messages:
        for message in messages:
            bot.send_message(chat_id, message)
    else:
        bot.send_message(chat_id, 'Пар с выгодным курсом не найдено')


@bot.message_handler(commands=['help'])
def help(message):
    chat_id = message.chat.id
    bot.send_message(chat_id, '/info - получение информации о выгодных курсах')
    bot.send_message(chat_id, '/profit - изменение разницы курсов')
    bot.send_message(chat_id, '/help - помощь')


@bot.message_handler(commands=['profit'])
def profit(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Введите разницу курсов в процентах, которую необходимо отслеживать')
    bot.register_next_step_handler(msg, profit_change)


def profit_change(message):
    chat_id = message.chat.id
    new_profit = message.text
    if new_profit.isdigit() and (int(new_profit) in range(0, 100)):
        bot.send_message(chat_id, 'Принято')
        db.change_profit(chat_id, int(new_profit))
    else:
        msg = bot.send_message(chat_id, 'Введите целое число от 0 до 100')
        bot.register_next_step_handler(msg, profit_change)


# Remove webhook, it fails sometimes the set if there is a previous webhook
bot.remove_webhook()

time.sleep(3)

# Set webhook
bot.set_webhook(url=WEBHOOK_URL_BASE+WEBHOOK_URL_PATH, certificate=open(WEBHOOK_SSL_CERT, 'r'))

# Start flask server
if __name__ == '__main__':
    app.run(host=WEBHOOK_LISTEN, port=WEBHOOK_PORT, ssl_context=(WEBHOOK_SSL_CERT, WEBHOOK_SSL_PRIV), debug=True)