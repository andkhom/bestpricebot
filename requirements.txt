pandas==0.22.0
psycopg2==2.7.3.2
pyTelegramBotAPI==3.5.2
requests==2.9.1
Flask==0.10.1
Flask-SSLify==0.1.5